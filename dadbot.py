# bot.py
import os

import discord
import requests
import string
import random
from dotenv import load_dotenv

load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')

if not TOKEN:
	TOKEN = os.environ.get('DISCORD_TOKEN')
	
BOT_ACKS = ["I heard my name!!", "So you want a joke, huh?", "Did someone say dad joke?"]	
BOT_FAIL_RESPONSES = ["Oh, well, I don't know any jokes.", "But I don't know any jokes.", "I'm tired, ask again some other time.", "Go ask your mother.", "What do I look like, a joke bot? Go away and let daddy drink.", "I'M TRYING TO POOP, LEAVE ME ALONE", "I'm not your real dad."]
JOKE_ABOUT_FAILURES = ["The joke slipped my mind", "It seems I can't recall anything about that", "What in tarnation is that?"]
JOKE_ABOUT_NOT_FOUND = ["I don't know any jokes about that.", "What kind of joke topic is that?", "That subject is nothing to joke about."]
SNARKY_RESPONSES = ["If you want a joke, look in the mirror. I'm tired. Get me a beer.", "The joke is my life. Fifty years on this planet and you are the only thing I have to show for it.", "Jokes are for winners. Go do something to actually make me proud and maybe I'll tell you a joke."]

JOKE_ABOUT_STRINGS = ["joke about", "jokes about"]

client = discord.Client()

def get_joke(**kwargs):
	search_url = "https://icanhazdadjoke.com/"
	if 'search_string' in kwargs:
		search_url = search_url + "search?term=" + str(kwargs.get('search_string'))
	headers = {'user-agent' : 'dadbot sephiroth1086@hotmail.com', 'Accept':'application/json'}
	response = requests.get(search_url, headers=headers)
	if response.status_code == requests.codes.ok:
		response_json = response.json()
		if 'joke' in response_json:
			return response_json["joke"]
		elif 'results' in response_json and len(response_json["results"]) != 0:
			return random.choice(response_json["results"])["joke"]
		
	return None

@client.event
async def on_ready():
    print(f'{client.user} has connected to Discord!')
	
@client.event
async def on_message(message):
	if message.author == client.user:
		return
		
	for s in JOKE_ABOUT_STRINGS:
		if s in message.content.lower():
			try:
				temp_string = message.content.lower()
				clean_string = temp_string.translate(str.maketrans('', '', string.punctuation))
				joke_subject = str(clean_string.split(s, 1)[1].split(' ')[1])
				print("attempting to find a joke about " + joke_subject)
				await message.channel.send("A joke about " + joke_subject + ", huh?")
				joke_response = get_joke(search_string=joke_subject)
				if joke_response is not None:
					await message.channel.send(joke_response)
				else:
					await message.channel.send(random.choice(JOKE_ABOUT_NOT_FOUND))
			except:
				print("Unexpected error while doing joke about something: ", sys.exc_info()[0])
				await message.channel.send(random.choice(JOKE_ABOUT_FAILURES))
				return
			return
			
	if 'joke' in message.content.lower() or 'dad' in message.content.lower():
		await message.channel.send(random.choice(BOT_ACKS))
		bot_response = random.choice(BOT_FAIL_RESPONSES)
		joke_response = get_joke()
		if joke_response is not None:
			bot_response = joke_response

		# mid life crisis extension.
		if random.randint(1,100) == 1:
			bot_response = random.choice(SNARKY_RESPONSES)
		
		await message.channel.send(bot_response)
		return


client.run(TOKEN)
